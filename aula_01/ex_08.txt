//Escreva um algoritmo em pseudocódigo que receba uma pilha de números inteiros e imprima a soma de todos os elementos da pilha
programa {
  funcao inicio() {
    inteiro tamanho = 5
    inteiro pilha[tamanho] = {3,4,5,6,7}

    inteiro soma = 0
    para (inteiro pos = tamanho-1; pos >= 0; pos--)
	{
      soma = soma + pilha[pos]
	}
    escreva(soma)
  }
}
