//Escreva uma função em pseudocódigo que receba dois números inteiros e retorne o seu máximo divisor comum
programa {
  funcao inicio() {
    escreva(mdc(12,20))
  }
  
  funcao inteiro mdc(inteiro a, inteiro b) {
  se (b == 0){
    retorne a
  }senao{
    retorne mdc(b, (a % b))
  }
}
}