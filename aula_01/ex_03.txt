//Escreva um algoritmo em pseudocódigo que receba um número inteiro positivo e imprima se ele é par ou ímpar
programa {
  funcao inicio() {
    inteiro numero
    escreva("Escreva um numero:")
    leia(numero)

    se(numero %2 == 0){
      escreva("Numero par")
    }senao{
      escreva("Numero ímpar")
    }
  }
}
