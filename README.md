# FIAP_dotnet_treinamento


## Objetivo

Repositório para armazenar os exercícios do curso de C# .NET da FIAP, promovido pelo Banco do Nordeste do Brasil S.A.

***

Link do curso: [Aulas .NET](https://gitlab.com/Delatorrea/curso-dotnet) 

## Aulas
- [X] [Aula 1](https://gitlab.com/suzanaporto/fiap_dotnet_treinamento/-/tree/main/aula_01?ref_type=heads) - Exercícíos em Pseudocódigo
- [X] [Aula 2](https://gitlab.com/suzanaporto/fiap_dotnet_treinamento/-/tree/main/aula_02?ref_type=heads) - Exercícíos de POO
- [X] [Aula 3](https://gitlab.com/suzanaporto/fiap_dotnet_treinamento/-/tree/main/aula_03?ref_type=heads) - Exercícíos de POO
- [X] [Aula 4](https://gitlab.com/suzanaporto/fiap_dotnet_treinamento/-/tree/main/aula_04?ref_type=heads) - Exercícíos de SOLID
