public class Calculadora : ICalculadora
{
    public double N1 { get; private set; }
    public double N2 { get; private set; }
    public double Resultado { get; private set; }

    public Calculadora()
    {}
    public Calculadora(double n1, double n2)
    {
        N1 = n1;
        N2 = n2;
    }

    public void Adicao()
    {
        Resultado = N1 + N2;
    }

    public void Divisao()
    {
        Resultado = N1 / N2;
    }

    public void Multiplicacao()
    {
        Resultado = N1 * N2;
    }

    public void Subtracao()
    {
        Resultado = N1 - N2;
    }
}