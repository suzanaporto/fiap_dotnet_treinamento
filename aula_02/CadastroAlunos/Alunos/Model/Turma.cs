class Turma : ITurma {
    public string Codigo { get; set; }
    public List<Aluno> Alunos { get; set; }

    public double Media { get; private set; }

    public Turma (string codigo){
        Codigo = codigo;
        Alunos = [];
    }

    public void CadastrarAlunos(Aluno aluno)
    {
        Alunos.Add(aluno);
        Console.WriteLine("Aluno: " + aluno.Nome + " Matriculado.");
    }

    public void CalcularMedia()
    {
        double soma = 0.0;
        foreach (var aluno in Alunos)
        {
            soma = soma += aluno.Nota;
        }
        Media = soma / Alunos.Count;
    }
    public void mostrarMedia()
    {
        Console.WriteLine("Média da Turma: " + Media);
    }
}