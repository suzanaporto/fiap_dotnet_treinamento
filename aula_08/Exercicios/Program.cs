﻿using System.Timers;

// Exercício 1: Uso Básico de Delegates
// Crie um delegate chamado Operation que aceita dois inteiros e retorna um inteiro. 
// Escreva métodos que correspondam à assinatura do delegate para adicionar, subtrair, multiplicar e dividir dois números. 
// Escreva um método que aceita o delegate Operation e chama o método referenciado.

Console.WriteLine("--Exercicio 1");
Operacoes(soma, 2, 3);

// Exercício 2: Funções Anônimas
// Modifique o exercício anterior para usar uma função anônima ao invés de métodos nomeados. Passe a função anônima diretamente para o método ExecuteOperation.
Console.WriteLine("--Exercicio 2");
Operacoes(delegate (int x, int y) { return x + y; }, 2, 3);

// Exercício 3: Expressões Lambda
// Refatore o exercício 2 para usar expressões lambda em vez de funções anônimas. 
// Demonstre a chamada do método ExecuteOperation com diferentes operações usando expressões lambda.
Console.WriteLine("--Exercicio 3");
Operacoes((x, y) => x * y, 2, 3);
Operacoes((x, y) => x + y, 2, 3);

// Exercício 4: Lambdas e Coleções
// Dada uma lista de strings, use uma expressão lambda e o método ForEach da lista para imprimir cada string em maiúsculas.
Console.WriteLine("--Exercicio 4");
List<string> nomes = new List<string> { "teste", "nome", "sobrenome"};
nomes.ForEach(nome => Console.WriteLine($"Nome: {nome.ToUpper()}"));

// Exercício 5: Lambdas e LINQ
// Utilize o LINQ para filtrar uma lista de números inteiros, retornando apenas os números pares.Use uma expressão lambda para definir o critério de filtro.
Console.WriteLine("--Exercicio 5");
List<int> numeros = new List<int> { 1,2,3,4,5,6,7,8,9,10};
Func<int, bool> eh_par = x => x % 2 == 0? true : false;
List<int> numeros_filtrados = numeros.Where(x => eh_par(x)).ToList();
numeros_filtrados.ForEach(numero => Console.Write($"{numero}, "));
Console.WriteLine("");

// Exercício 6: Lambdas e Eventos
// Crie um simples temporizador (usando a classe System.Timers.Timer) que dispara um evento a cada segundo. 
// Use uma expressão lambda para se inscrever no evento e imprimir a hora atual a cada tick do temporizador.
Console.WriteLine("--Exercicio 6");
System.Timers.Timer timer = new System.Timers.Timer(1000);
timer.Elapsed += (sender, e) => Console.WriteLine($"Tick: {DateTime.Now}");
timer.Start();

int soma(int x, int y) { return x + y;};
int sub(int x, int y) { return x - y;};
int mult(int x, int y) { return x * y;};
int divisao(int x, int y) { return x / y;};

void Operacoes(Operation op, int x, int y){
    int resultado = op(x,y);
    Console.WriteLine($"Resultado: {resultado}");
}

delegate int Operation(int x, int y);

