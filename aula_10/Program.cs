﻿// Exercicio 1: Consulta de Alunos em Memória: Ajuste o exemplo anterior para incluir mais propriedades nos alunos 
// e realize consultas mais complexas, como encontrar alunos de uma turma específica que também passaram em todas as matérias.
var alunos = new List<Aluno>
{
    new("João","A",18, new DateTime(1993,12,12)),
    new("Maria","B",18, new DateTime(1995,01,12)),
    new("Pedro","A",18, new DateTime(1994,03,12)),
};

alunos[0].passouDeAno = true;
alunos[1].passouDeAno = false;
alunos[2].passouDeAno = true;

var alunosDaTurmaB = alunos.Where(aluno => aluno.Turma == "B" && aluno.Nome == "Maria");
var alunosTurma = alunos.GroupBy(aluno => aluno.Turma).ToList();

foreach (var aluno in alunosTurma)
{
    foreach(var x in aluno){
        Console.WriteLine(x.Nome);
    }
}

// Exercicio 2: Consulta de Dados em Arquivos: Tente processar um arquivo CSV ou TXT mais complexo, 
// que inclua múltiplas colunas e linhas, e extraia informações específicas usando LINQ.
var linhas = File.ReadAllLines("./Data/MOCK_DATA.csv");
var header = linhas.Take(1);
var todasAsLinhas = linhas.Skip(1);
var nomes = linhas.Select(linha => linha.Split(',')[1]).Where(linha => linha.Contains("John")); // Supondo que o nome esteja na primeira coluna
var qtde_homens = linhas.Select(linha => linha.Split(',')[4]).Where(linha => linha == "Male").Count(); // Supondo que o nome esteja na primeira coluna
var qtde_mulheres = linhas.Select(linha => linha.Split(',')[4]).Where(linha => linha == "Female").Count(); // Supondo que o nome esteja na primeira coluna

Console.WriteLine($"Quantidade de homens: {qtde_homens}");
Console.WriteLine($"Quantidade de mulheres: {qtde_mulheres}");

Console.WriteLine("---Cabeçalho");
foreach(var x in header){
    Console.WriteLine(x);
}
Console.WriteLine("---Nomes");
foreach (var nome in nomes)
{
    Console.WriteLine(nome);
}
// Console.WriteLine("---Todas as linhas sem Cabeçalho");
// foreach(var x in todasAsLinhas){
//     Console.WriteLine(x);
// }

class Aluno {
    public string Nome { get; set; }
    public string Turma { get; set; }
    public int Idade { get; set; }
    public DateTime DataMatricula  { get; set; }
    public bool passouDeAno {get; set;}

    public Aluno(string nome, string turma, int idade, DateTime DataMatricula)
    {
        Nome = nome;
        Turma = turma;
        Idade = idade;
        this.DataMatricula = DataMatricula;
    }
}

