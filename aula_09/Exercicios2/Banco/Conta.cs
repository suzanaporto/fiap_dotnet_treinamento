class Agencia {

    public string nome { get; set; }

    public Agencia(string nome)
    {
        this.nome = nome;
    }
  
    public string criarConta(Cliente c) {
        List<string> devedores = GeradorDeDevedores.GerarLista();
        // Exercicio 6: Verificar lentidao 
        for (int i = 0; i < 30000; i++)
        {
        foreach (var nome in devedores)
        {
            if(c.nome == nome){
                throw new Exception("Nome na lista de devedores");
            }
        }
        }
        return "Conta Criada";
    }
    public string criarContaHashSet(Cliente c) {
        HashSet<string> devedores = GeradorDeDevedores.GerarHashSet();
        // Exercicio 6: Verificar lentidao 
        for (int i = 0; i < 30000; i++)
        {
        foreach (var nome in devedores)
        {
            if(c.nome == nome){
                throw new Exception("Nome na lista de devedores");
            }
        }
        }
        return "Conta Criada";
    }

}