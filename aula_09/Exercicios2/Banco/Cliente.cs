class Cliente {
    public string nome { get; set; }
    public int idade { get; set; }

    public Cliente(string nome, int idade)
    {
        this.nome = nome;        
        this.idade = idade;        
    }

}