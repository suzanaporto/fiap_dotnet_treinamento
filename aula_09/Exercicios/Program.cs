﻿// Exercício 1: Crie uma lista de strings e adicione os nomes de alguns programadores famosos. Em seguida, remova o primeiro nome da lista e adicione "Ada Lovelace" no início.
List<string> programadores = new List<string>{ "Alan Turing", "Grace Hopper", "Linus Torvalds"};
programadores.RemoveAt(0);
programadores.Insert(0, "Ada Lovalace");
foreach (var prog in programadores){
    Console.WriteLine(prog);
}
// Exercício 2: Use uma pilha para simular a operação de navegação para frente e para trás em um navegador da web.
Stack<string> paginas = new Stack<string>();
paginas.Push("pagina1");
Console.WriteLine("Indo para pagina 1");
Console.WriteLine(paginas.Peek());
paginas.Push("pagina2");
Console.WriteLine("Indo para pagina 2");
Console.WriteLine(paginas.Peek());
Console.WriteLine("Voltando");
paginas.Pop();
Console.WriteLine(paginas.Peek());

// Exercício 4: Crie um dicionário para armazenar as notas de uma turma, onde a chave é o nome do aluno e o valor é a nota.
Dictionary<string, double> notas = new();
notas.Add("João",  7.6);
notas.Add("Maria",  8.6);
notas.Add("José",  9.6);
double nota = notas["João"];

// Exercício 3: Implemente uma fila para gerenciar uma fila de impressão, onde cada documento tem um nome e uma quantidade de páginas.
Queue<Doc> fila_documentos = new Queue<Doc>();
fila_documentos.Enqueue(new Doc("teste", 12));
fila_documentos.Enqueue(new Doc("Nome", 3));
class Doc {
    public string Nome;
    public int Qtde_paginas;

    public Doc(string n, int paginas)
    {
        Nome = n;
        Qtde_paginas = paginas;
    }
}
